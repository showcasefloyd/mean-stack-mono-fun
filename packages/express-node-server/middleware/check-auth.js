const jwt = require("jsonwebtoken");
const Secret = require("../secret");

// Check for valid token

// export this as a function
module.exports = (req,res,next) => {

  // if there is no header, the try will throw and err to catch
  try {

    // Split and take second argument
    const token = req.headers.authorization.split(" ")[1];
    const decodedToken = jwt.verify(token, Secret);
    req.userData = {email: decodedToken.email, userId: decodedToken.userId}
    next();

  } catch (err) {

    res.status(401).json({
      message: "Unauthorized. Please log in"
    })

  }


}