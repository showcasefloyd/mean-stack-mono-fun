const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

// Create a Schema
// Types are uppercase
const userSchema = mongoose.Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
})

userSchema.plugin(uniqueValidator);

// Now we create a model from the schema
module.exports = mongoose.model('User', userSchema);