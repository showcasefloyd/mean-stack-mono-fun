const mongoose = require('mongoose');

// Create a MonGoose Schema. Note: Types are in uppercase
const bookSchema = mongoose.Schema({
  title: { type: String, required: true },
  description: { type: String, required: true },
  imagePath: { type: String, required: true },
  creator: { 
    type:  mongoose.Schema.Types.ObjectId, 
    ref: "User"
  },
  email: { 
    type:  mongoose.Schema.Types.String, 
    ref: "User"
  },
})

// Now we create a model from the schema
module.exports = mongoose.model('Book', bookSchema);