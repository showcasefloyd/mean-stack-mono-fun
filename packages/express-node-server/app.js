const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const mongoose = require("mongoose");
const path = require('path');

const booksRoutes = require('./routes/books')
const userRoutes = require('./routes/user')

mongoose
  .connect("mongodb://admin:password@localhost:27017", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(
    () => {
      console.log("Connected to mongodb");
    },
    (err) => {
      console.error.bind(console, "Connection error:");
    }
  );
  
// Method for extracting the body of an incoming http POST request
app.use(bodyParser.json());
app.use("/uploads/images", express.static(path.join('uploads/images')));

// Headers for our Express server
app.use((rep, res, next) => {
  // Fixes Cors Error Issue so the request will work
  res.setHeader("Access-Control-Allow-Origin", "*");
  // Adds some additional headers we may want to include
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-Header, Content-Type, Accept, Authorization"
  );
  // The actions we allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  // Just passes the function to the next filter
  next();
});


// Add our routes from another file
// Can also include the first route here as an extra parameter
// Examples: pp.use("/api/books", booksRoutes)
app.use("/api/books", booksRoutes);
app.use("/api/user", userRoutes);


module.exports = app;
