const express = require("express");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const router = express.Router();
const User = require('../models/user');
const Secret = require("../secret");

router.post("/signup", async(req, res, next) => {

   // creating a new mongoose doc from user data
   const user = new User({
      email: req.body.email,
      password: req.body.password
    });

   // generate salt to hash password
   const salt = await bcrypt.genSalt(10);
   // now we set user password to hashed password
   user.password = await bcrypt.hash(user.password, salt);

   user
    .save()
    .then((result) => res.status(201).json({
        message: 'New user created',
        result: result,
      })
    )
    .catch(err => {
      res.status(500).json({
        error: err
      })
    });

});

router.post("/login",(req, res, next) => {

  let fetchedUser;

  User.findOne({ email: req.body.email})
    .then(user => {
      if(!user){
        return res.status(401).json({
          message: "Auth failed"
        })
      }

      fetchedUser = user;
      // Email found, check password
      return bcrypt.compare(req.body.password, user.password)
    })
    .then(result => {

      if(!result){
        return res.status(401).json({
          message: "Auth failed"
        })
      }

      console.log("Secret ", Secret)

      // Password was found, creat token
      const token = jwt.sign(
        {
          email: fetchedUser.email,
          userId: fetchedUser._id
        }, 
        Secret, 
        { expiresIn: '10m'}
      )

      res.status(200).json({
        token: token,
        expiresIn: "600" // seconds 60 * 10m
      })
    
    })
    .catch(err => {
      return res.status(500).json({
        error: err
      })
    })

})


module.exports = router;
