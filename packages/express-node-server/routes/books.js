const express = require('express');
const router = express.Router();
const multer = require("multer");
const checkAuth = require("../middleware/check-auth");

// Our Mongo model
const Book = require("../models/book");

const MIME_TYPE_MAP = {
  'image/png': 'png',
  'image/jpeg': 'jpg',
  'image/jpg': 'jpg',
}

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads/images")
  },
  filename: (req, file, cb) => {
    const name = file.originalname.toLowerCase().split(' ').join('-');
    const ext = MIME_TYPE_MAP[file.mimetype];
    cb(null, name + '.' + ext);
  }
})
// New

router.post("", checkAuth, multer({storage: storage}).single("image") ,(req, res, next) => {

  const url = req.protocol + "://" + req.get('host');
  
  const book = new Book({
    title: req.body.title,
    description: req.body.description,
    imagePath: url + "/uploads/images/" + req.file.filename,
    creator: req.userData.userId,
    email: req.userData.email,
  });

  book.save().then((createdBook) => {
    // Return a response was successful the Json message is just optional
    // 201 is status for new item added
    res.status(200).json({
      messages: "Book successfully added to database",
      book : {
        id: createdBook._id,
        title: createdBook.title,
        description: createdBook.description,
        imagePath:  createdBook.imagePath,
        creator: createdBook.creator,
        email: createdBook.email
      }
    });
  });
});

// Update
router.put("/:id", checkAuth, multer({storage: storage}).single("image"), async (req, res, next) => {
  
  const url = req.protocol + "://" + req.get('host');
  let fn;
 
  if(req.file){
    fn = url + "/uploads/images/" + req.file.filename
  } else {
    fn = req.body.imagePath;
  }

  const book = {
    id: req.params.id,
    title: req.body.title,
    description: req.body.description,
    imagePath: fn
  };

  // Mongoose routes return a Promise
  try {
    const response = await Book.updateOne({ _id: book.id }, book);
    if (response) {
      res.status(200).json({
        messages: "3 Express -  Books updated in database ",
        book : {
          id: response.id,
          title: response.title,
          description: response.description,
          imagePath: response.imagePath
        }
      });
    }
  } catch (err) {
    res.status(400).json({
      messages: "Error updating the database " + err,
    });
  }
});

router.get("/:id", async (req, res, next) => {
  try {
    const book = await Book.findOne({ _id: req.params.id });
    res.status(200).json({
      messages: "Book with id " + req.params.id + " successfully from database",
      book: book,
    });
  } catch (err) {
    console.log("Error retrieving book ", err);
  }
});

// Get All 
router.get("", (req, res, next) => {

  // The plus sign converts the string to a number
  const pageSize = +req.query.pageSize;
  const currentPage = +req.query.currentPage;

  // Set up our Mongoose Query -- Book as our Model
  const booksQuery = Book.find();
  let fetchBooks;
  
  // Pagination
  if(pageSize && currentPage){
    console.log(req.query)
    // Example: http://localhost:3000/api/books?pageSize=2&currentPage=1
    booksQuery.skip(pageSize * (currentPage - 1)).limit(pageSize)
  }

  // get count and pass it to next then statement
  booksQuery
    .then(books => {
      fetchBooks = books;
      return Book.count()
    })
    .then(count => {
      res.status(200).json({
        messages: "Books fetched successfully from database!",
        books: fetchBooks,
        total: count
      });
    })

});

router.delete("/:id", checkAuth, (req, res, next) => {
  Book.deleteOne({ _id: req.params.id }).then((result) => {
    res.status(200).json({
      messages: "Book deleted from database",
    });
  });
});

router.use((req, res, next) => {
  res.send("Hello from Express");
});

module.exports = router;