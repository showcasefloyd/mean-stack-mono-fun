import { Component, OnInit } from '@angular/core';
import { AuthService } from './books/auth/auth.service';

import { Book } from './books/book.interface'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  public catalog: Book[] = [];

  constructor(private authService: AuthService){}

  ngOnInit(){
    this.authService.autoAuth()
  }

}
