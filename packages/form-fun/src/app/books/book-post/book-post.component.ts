import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { BooksService } from '../books.service';
import { Book } from '../book.interface';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

@Component({
  selector: 'app-book-post',
  templateUrl: './book-post.component.html',
  styleUrls: ['./book-post.component.scss'],
})

/** 
 * Using to Template Driven Forms (Forms Module) 
 * Note: This Form was replaced by the BookForms Component
 * and is not longer in use. I'm just keeping it for reference
 * 
*/

export class BookPostComponent implements OnInit {

  book: Book;
  private editBookMode = false;
  private bookId: string;

  addBook(form: NgForm): void {
    if (form.invalid) {
      return;
    }
    
    const book: Book = {
      id: this.bookId,
      title: form.value.title,
      description: form.value.description,
      imagePath: form.value.image,
    };

    if(this.editBookMode){
      this.booksService.editBook(book);
    } else {
      this.booksService.addBook(book);
    }
  }

  constructor(
    public booksService: BooksService,
    public route: ActivatedRoute
  ) {}

  ngOnInit() {

    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if(paramMap.has('id')){

        this.editBookMode = true;
        this.bookId = paramMap.get('id');
        this.book = this.booksService.getBook(this.bookId);
      } else {

        this.editBookMode = false;
        this.bookId = null;
      }
    });
  }
}
