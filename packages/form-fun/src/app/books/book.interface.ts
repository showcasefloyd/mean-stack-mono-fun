
export interface Book {
  _id?: string,
  id: string,
  title: string;
  description: string;
  imagePath: string;
  creator?: string;
  email?: string;
}