import { Component, } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login-component',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

/** Using to Template Driven Forms (Forms Module) */

export class LoginComponent {
    
    isLoading = false;
    form: FormGroup;
    passwordInvalid: boolean;
    emailInvalid: boolean;

    constructor(private authService: AuthService){}

    onLogin(form: NgForm){
      
      this.isLoading = true;
      const email = form.value.email;
      const password = form.value.password;
      
      if(email === ""){
        this.emailInvalid = true;
      } else {
        this.emailInvalid = false;
      }

      if(password === ""){
        this.passwordInvalid = true;
      } else {
        this.passwordInvalid = false;
      }

      if(password !== "" && email !== ""){
        this.authService.loginUser(email,password);
      }

    }
        
}        
      