import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

import { AuthData } from "./auth-data.model"

interface JWT  {
  token: string;
  expiresIn: number | Date;
}

@Injectable({ providedIn: 'root' })
export class AuthService {

  private token: JWT['token'];
  private isAuthenticated = false; 
  private authStatus = new Subject<boolean>();
  private timeout: any;

  getToken(): string {
    return this.token;
  }

  getIsAuthenticated(): boolean {
    return this.isAuthenticated;
  }

  getAuthStatus(){
    return this.authStatus.asObservable();
  }

  loginUser(email: string, password: string): void {
    const authData: AuthData = {email: email, password: password};

    this.httpClient.post<JWT>("http://localhost:3000/api/user/login", authData)
      .subscribe(response => {
        this.token = response.token;
    
        if(this.token){
          const expiresInDuration = response.expiresIn as number;
      
          this.isAuthenticated = true;
          this.authStatus.next(true);
          this.setAuthTime(expiresInDuration);
          
          const now = new Date();
          const expirationDate = new Date(now.getTime() + expiresInDuration * 1000);
          this.saveAuthData(this.token, expirationDate)

          this.router.navigate(["/"]);
        }
      })

  }

  signupUser(email: string, password: string): void {
    const authData: AuthData = {email: email, password: password};

    this.httpClient.post("http://localhost:3000/api/user/signup", authData)
      .subscribe(response => {
        console.log(response);
        this.authStatus.next(true);
        this.router.navigate(["/"])
      })

  }

  logoutUser(): void {
    this.token = null; // Clear the token
    this.clearAuthData(); // Clean local storage
    clearTimeout(this.timeout); // End Timeout if user logs out first
    this.authStatus.next(false);
    this.router.navigate(["/"]);
  }

  autoAuth() {

    const authInformation = this.getAuthData();
    if(!authInformation){
      return;
    }
    const now = new Date();
    const future = new Date(authInformation.expiresIn)
    const isInFuture = future.getTime() - now.getTime();
    
    if(isInFuture > 0){
      this.token = authInformation.token;
      this.isAuthenticated = true;
      this.setAuthTime(isInFuture / 1000);
      this.authStatus.next(true);
    }
  }

  private setAuthTime(duration: number){
    console.log("Setting duration " + duration);
    this.timeout = setTimeout(() => {
      this.logoutUser();
    },duration * 1000)
  }

  private saveAuthData(token: JWT['token'], expirationDate: Date){

    localStorage.setItem("token", token);
    localStorage.setItem("expiration", expirationDate.toISOString());

  }

  private clearAuthData(): void {

    localStorage.removeItem("token");
    localStorage.removeItem("expiration");
  }

  private getAuthData(): JWT {

    const token = localStorage.getItem("token");
    const expiresIn = localStorage.getItem("expiration");

    if( !token || !expiresIn){
      return;
    } else {
      return {
        'token': token,
        'expiresIn': new Date(expiresIn)
      }
    }

  }

  constructor(
    private httpClient: HttpClient,
    private router: Router
  ){}

}