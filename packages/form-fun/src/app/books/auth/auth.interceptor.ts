import { HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from "./auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authServer: AuthService){}

  intercept(req: HttpRequest<any>, next: HttpHandler){

    // grab token from AuthService
    const authToken = this.authServer.getToken();

    //console.log(authToken)

    const authRequest = req.clone({
      headers: req.headers.set('Authorization', 'Bearer ' + authToken)
    })
    return next.handle(authRequest);
  }

}