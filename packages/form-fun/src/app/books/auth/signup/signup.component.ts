import { Component, } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup-component',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})

/** Using to Template Driven Forms (Forms Module) */

export class SignupComponent {
  
  isLoading = false;
  form: FormGroup;
  passwordInvalid: boolean;
  emailInvalid: boolean;

  constructor(private authService: AuthService){}

  onSignup(form: NgForm){

    this.isLoading = true;
    const email = form.value.email;
    const password = form.value.password;

    //console.log("Sign Up", form.value)
    
    if(email === ""){
      this.emailInvalid = true;
    } else {
      this.emailInvalid = false;
    }

    if(password === ""){
      this.passwordInvalid = true;
    } else {
      this.passwordInvalid = false;
    }

    if(password !== "" && email !== ""){
      this.authService.signupUser(email,password);
    }  
  }     

}