import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { MatCard } from '@angular/material/card';
import { MatFormField  } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input'

import { BookFormComponent } from './book-form.component';
import { BooksService } from '../books.service';

describe('The Book Form Component (BookFormComponent)', () => {

  let component: BookFormComponent;
  let fixture: ComponentFixture<BookFormComponent>;
  let el: DebugElement;

  beforeEach(waitForAsync (() => {
     TestBed.configureTestingModule({
      declarations: [ 
        BookFormComponent,
        MatCard,
        MatFormField
      ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MatInputModule,
        NoopAnimationsModule,
        ReactiveFormsModule
      ],
      providers: [
        BooksService
      ]
    })
    .compileComponents()
    .then(() => {

      fixture = TestBed.createComponent(BookFormComponent);
      component = fixture.componentInstance;
      el = fixture.debugElement;

      fixture.detectChanges();      
    });

  }));

  it('should create itself', () => {

    expect(component).toBeTruthy();
    
  });
});
