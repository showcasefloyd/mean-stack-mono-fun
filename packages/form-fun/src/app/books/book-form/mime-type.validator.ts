import { AbstractControl } from "@angular/forms";
import { Observable, Observer } from "rxjs";

type Generic = {
  [key: string]: any;
}

export const mineType = (control: AbstractControl): Promise<Generic> | Observable<Generic> => {

  const file = control.value as File;
  const fileReader = new FileReader();

  const frObs = new Observable((observer: Observer<Generic>) => {
      
    fileReader.addEventListener("loadend", () => {

      const arr = new Uint8Array(fileReader.result as ArrayBuffer).subarray(0,4);
      let header = "";
      let isValid = false;

      for(let i = 0; i < arr.length; i++){
        header += arr[i].toString(16);
      }

      switch (header) {
        case "89504e47":
          isValid = true;
          break;
        case "ffd8ffe0":
        case "ffd8ffe1":
        case "ffd8ffe2":
        case "ffd8ffe3":
        case "ffd8ffe8":
          isValid = true;
          break;
        default:
          isValid = false; 
          break;
      }
      
      if(isValid){
        observer.next(null); //true
      } else {
        observer.next({ invalidType: true }) // false
      }

      observer.complete();

    });

    fileReader.readAsArrayBuffer(file)

  });

  return frObs;

};