import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';

import { BooksService } from '../books.service';
import { Book } from '../book.interface';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { mineType } from './mime-type.validator';

/** Switched to Reactive Forms */

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.scss'],
})
export class BookFormComponent implements OnInit {
  
  book: Book;
  form: FormGroup;
  isLoading = false;
  imagePreview: string;
  formState: string;

  private editBookMode = false;
  private bookId: string;

  // Add or Edit a Book
  addBook(): void {

    if (this.form.invalid) {
      return;
    }
    
    const book: Book = {
      id: this.bookId,
      title: this.form.value.title,
      description: this.form.value.description,
      imagePath: this.form.value.image
    };

    if(this.editBookMode){
      this.booksService.editBook(book);
    } else {
      this.booksService.addBook(book);
    }
  
    this.form.reset();
  }

  onImagePicked(event: Event) {

    // type conversion from event to an HTMLInputElement
    // then garb first image from images array
    const file = (event.target as HTMLInputElement).files[0];

    // update the form element
    this.form.patchValue({'image': file});
    this.form.get('image').updateValueAndValidity();

    // FileRead: 1 Create instance of File reader
    const reader = new FileReader()

    // FileRead: 2 Assign what happens when you done loading the file
    reader.onload = () => {
      this.imagePreview  = reader.result as string;
    }

    // FileRead: 3 Load the file
    reader.readAsDataURL(file);

  }

  constructor(
    public booksService: BooksService,
    public route: ActivatedRoute
  ) {}

  ngOnInit() {

    this.form = new FormGroup(
      {
        'title': new FormControl( 
          null, {validators: [Validators.required, Validators.minLength(3)] }
        ),
        'description': new FormControl(
          null, {validators: [Validators.required] }
        ),
        'image': new FormControl(
          null, {validators: [Validators.required], asyncValidators: [mineType] }
        ) 
      }
    );

    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      
      if(paramMap.has('id')){
        this.formState = 'Edit';

        this.editBookMode = true;
        this.bookId = paramMap.get('id');
        this.book = this.booksService.getBook(this.bookId);

        this.form.setValue({
          'title': this.book.title, 
          'description': this.book.description,
          'image': this.book.imagePath
        })

        this.imagePreview = this.book.imagePath;

      } else {

        this.formState = 'Add New';
        this.editBookMode = false;
        this.bookId = null;
      }

    });
  }
}
