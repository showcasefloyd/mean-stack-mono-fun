import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatExpansionModule } from '@angular/material/expansion';

import { BookListComponent } from './book-list.component';
import { BooksService } from "../books.service";

describe('Book List Component Test (app-book-list)',() => {

  let component: BookListComponent;
  let fixture: ComponentFixture<BookListComponent>;
  let el: DebugElement;
  let booksServiceSpy: any;

  beforeEach(waitForAsync(() => {

    TestBed.configureTestingModule({
      declarations: [
        BookListComponent,
      ],
      imports: [
        MatToolbarModule,
        MatPaginatorModule,
        MatExpansionModule,
        HttpClientTestingModule,
        RouterTestingModule,
        NoopAnimationsModule
      ],
      providers: [BooksService],
    })
    .compileComponents()
    .then(() => {
      fixture = TestBed.createComponent(BookListComponent);
      component = fixture.componentInstance;
      el = fixture.debugElement;

      //booksServiceSpy = jasmine.createSpyObj('BooksService',["getBooks"]);

      fixture.detectChanges();
    })
  
  }));

  it('should be able to create itself',() => {

    expect(component).toBeTruthy();

  })

  it('should contain a list of books to read',() => {

    pending();

  })
});