import { ThisReceiver } from "@angular/compiler";
import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { PageEvent } from "@angular/material/paginator";
import { Subscribable, Subscription } from "rxjs";
import { AuthService } from "../auth/auth.service";

import { Book } from '../book.interface';
import { BooksService } from "../books.service";

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit, OnDestroy {
    
  books: Array<Book> = [];
  isLoading: Boolean;

  totalBooks: Number;
  booksPerPage = 2;
  bookSizeOptions = [1,2,5,10];

  currentPage = 1;
  userIsAuth = false; 

  private booksSub: Subscription;
  private authSub: Subscription
  
  constructor(
    public booksService: BooksService, 
    private authService: AuthService
  ){}
  
  ngOnInit(){

    // Calls the service for us the first time, which should trigger
    // the next in the Observable below.
    this.booksService.getBooks(this.booksPerPage, this.currentPage);

    // Show spinner
    this.isLoading = true
    
    // Subscribed can take three functions next(), err() and end()
    this.booksService.getBooksUpdated().subscribe((postData) => {
        this.isLoading = false;
        this.books = postData.books
        this.totalBooks = postData.booksCount;
      }
    );

    this.userIsAuth = this.authService.getIsAuthenticated();

    this.authSub = this.authService
      .getAuthStatus()
      .subscribe(isAuthenticated => {
        this.userIsAuth = isAuthenticated;
      });
  }

  ngOnDestroy(){
    this.authSub.unsubscribe()
  }

  delBook(id: string): void {
    this.booksService.deleteBook(id);
  }

  onPageChange(booksListData: PageEvent){
    this.currentPage = booksListData.pageIndex + 1;
    this.booksPerPage = booksListData.pageSize;
    this.booksService.getBooks(this.booksPerPage, this.currentPage);
  }

}

