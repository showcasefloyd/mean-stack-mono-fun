import { Injectable, ɵConsole } from '@angular/core';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { Book } from './book.interface';
import { Router } from '@angular/router';

interface BooksResults { 
  message: String;
  books:[Book];
  total: Number;
}

// Now available to components
@Injectable({ providedIn: 'root' })
export class BooksService {

  private books: Book[] = [];
  private booksUpdated = new Subject<{books: Book[]; booksCount: Number}>();
  // Total in Database
  maxBooks: Number;

  getBooks(postsPerPage: number, currentPage: number) {

    // Make books immutable
    // The get method is doing a lot for us under the hood
    // 1. It returns not a fetch but an Angular Observable - nice
    // 2. It handles take the Json and turing it back in the JavaScript for us
    // 3. It handles the ngOnDestroy to clean up a bit us so there are no
    // memory leaks

    const queryParams = `?pageSize=${postsPerPage}&currentPage=${currentPage}`

    this.httpClient
      .get<BooksResults>(
        'http://localhost:3000/api/books' + queryParams
      )
      .pipe(map((jsonBody) => {
        return {
          books: jsonBody.books.map((book) => {
            return {
              id: book._id,
              title: book.title,
              description: book.description,
              imagePath: book.imagePath,
              creator: book.creator,
              email: book.email
            }
          }),
          total: jsonBody.total
        }  
      }))
      .subscribe((jsonBody) => {
        console.log(jsonBody)
        this.books = jsonBody.books;
        this.maxBooks = jsonBody.total;

        // this next is important. It informs the Angular service that new
        // data exists and is ready to be used.
        this.booksUpdated.next({books: [...this.books], booksCount: this.maxBooks});
      });
  }

  getBook(id: string): Book {
   // No need for a call to the database since we already have all the
   // books in our service
   return {...this.books.find(n => n.id === id)};
  }

  getBooksUpdated() {
    // We don't return the private Subject, instead we return a new 
    // Observable which the component can subscribe to next(), err() and end()

    return this.booksUpdated.asObservable();
  }

  editBook(book: Book): void {
    let updatedBook

    if(typeof(book.imagePath) === 'object'){

      updatedBook = new FormData();
      updatedBook.append('id', book.id);
      updatedBook.append('title', book.title);
      updatedBook.append('description', book.description);
      updatedBook.append('image', book.imagePath, book.title);

    } else {

      updatedBook = {
        id: book.id,
        title: book.title,
        description: book.description,
        imagePath: book.imagePath
      };
    }
    
    // httpClient returns an Observable
    this.httpClient
    .put(
      'http://localhost:3000/api/books/' + book.id,
      updatedBook
    )
    .subscribe(
      (response) => {
      this.router.navigate(['/'])
    },
    (err) => console.error('Observer got an error: ' + err),
    ()=>{}
    )
  }

  addBook(book: Book): void { 
    // Create form data group to append data to the form
    // which includes the file upload
    const postData = new FormData();
    postData.append('title', book.title);
    postData.append('description', book.description);
    postData.append('image', book.imagePath, book.title);

    this.httpClient
      .post<{ message: string, bookId: string }>(
        'http://localhost:3000/api/books', 
        postData
      )
      .subscribe((repBody) => {
        this.router.navigate(['/'])
      });
  }

  deleteBook(bookId: string): void {
    this.httpClient.delete('http://localhost:3000/api/books/' + bookId)
      .subscribe( () => {
        this.books = this.books.filter(bk => bk.id != bookId);
        this.booksUpdated.next({books: [...this.books], booksCount: this.maxBooks});
      });
  }

  constructor(private httpClient: HttpClient, private router: Router) {}
}
