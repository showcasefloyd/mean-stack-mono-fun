import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookListComponent } from './books/book-list/book-list.component';
import { BookFormComponent } from './books/book-form/book-form.component';
import { LoginComponent } from './books/auth/login/login.component';
import { SignupComponent } from './books/auth/signup/signup.component';
import { AuthGuard } from './books/auth/auth.guard';

const routes: Routes = [
  { path: "", component: BookListComponent },
  { path: "create", component: BookFormComponent, canActivate: [AuthGuard] },
  { path: "edit/:id", component: BookFormComponent, canActivate: [AuthGuard] },
  { path: "login", component: LoginComponent},
  { path: "signup", component: SignupComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
