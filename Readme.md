# MEAN Stack

## Monorepo that contains the different parts of my MEAN stack

### Based on the Udemy Course [Angular and Node JS](https://tfs.udemy.com/course/angular-2-and-nodejs-the-practical-guide/)

This project is designed around Yarn workspaces. All the code resides in the `/packages` directory as follows

    /express-node-server
    /form-fun
    /mongo

To start local environment you must fire up 3 services. Mongo, Express and for the frontend development Angular Development Server

- Mongo

    cd into projects Mongo and run `docker compose up`

- ExpressJS

    `yarn workspace express-node-server start`

- Angular Development Server

    `yarn workspace form-fun start`


## ExpressJS
The ExpressJS backend runs on port [:3000](http://localhost:3000)

## Angular Dev Server 
The Book List Demo runs on port [:4200](http://localhost:4200)

## Monogo Express Web App 
Web based UI for Mongo DB [:8081](http://localhost:8081)

## Yarn Run Commands
These bottom two commands are also documented in the package.json file as follows


    "serverUp" : "yarn workspace express-node-server start",
    "appUp" : "yarn workspace form-fun start",
    "start" : "concurrently --kill-others-on-fail yarn serverUp yarn appUp